﻿using MicroIMDBV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MicroIMDBV2.Controllers
{
    public class MovieController : Controller
    {
        // GET: Movie
        public ActionResult Index()
        {
            using (var db = new MovieDBContext())
            {
                var movies = db.Movies.ToList();
                return View(movies);
            }
        }
        [HttpPost]
        public ActionResult Add(Movie movieFromView)
        {
            using (var db = new MovieDBContext())
            {
                db.Movies.Add(movieFromView);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}